using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR;

public class testtag3 : MonoBehaviour
{
    private XRSocketInteractor interactor;
    public GameObject correct;
    public GameObject wrong;


    void Start()
    {
        interactor = GetComponent<XRSocketInteractor>();
    }

    void OnTriggerEnter(Collider other)
    {
        // Check if the colliding object has the tag "6"
        if (other.CompareTag("3"))
        {
            correct.SetActive(true);
            wrong.SetActive(false);
        }
        if (other.CompareTag("2") || other.CompareTag("1") || other.CompareTag("4") || other.CompareTag("5"))
        {
            wrong.SetActive(true);
            correct.SetActive(false);
        }
    }
}
